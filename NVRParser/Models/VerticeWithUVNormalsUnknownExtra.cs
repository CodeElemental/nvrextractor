﻿namespace NvrParser.Models
{
    public class VerticeWithUVNormalsUnknownExtra : VerticeWithUVNormalsUnknown
    {
        public byte[] Extra { get; set; } 

        public string getReadableExtra()
        {
            return Extra[0] + " " + Extra[1] + " " + Extra[2] + " " + Extra[3];
        }
    }
}
