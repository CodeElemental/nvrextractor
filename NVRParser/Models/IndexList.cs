﻿using System.Collections.Generic;

namespace NvrParser.Models
{
    public class IndexList
    {
        public int Size { get; set; }
        public uint D3DFormat { get; set; }
        public List<ushort> Indexes { get; set; }

        public IndexList()
        {
            Indexes = new List<ushort>();
        }

        public void AddIndex(ushort index)
        {
            Indexes.Add(index);
        }

    }
}
