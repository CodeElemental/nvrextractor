﻿namespace NvrParser.Models
{
    public class VerticeWithUVNormals : Vertice
    {
        public float[] Normals { get; set; } // Lenght 3
        public float[] UV { get; set; } // Lenght 2
    }
}
