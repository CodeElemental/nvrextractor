﻿namespace NvrParser.Models
{
    public class VerticeWithUVNormalsUnknown : VerticeWithUVNormals
    {
        public byte[] Unknown { get; set; } // Lenght 4

        public string getUnknownReadable()
        {
            string result = "";
            result += Unknown[0] + " " + Unknown[1] + " " + Unknown[2] + " " + Unknown[3];
            return result;
        }
    }
}
