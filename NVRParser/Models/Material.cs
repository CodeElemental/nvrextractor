﻿namespace NvrParser.Models
{
    public class Material
    {
        public string MaterialName { get; set; } // Lenght 256
        public int[] EmissiveColor { get; set; } // Lenght 3
        public float[] BlendColor { get; set; } // Lenght 4
        public string TextureFileName { get; set; } // Lenght 336
        public float Opacity { get; set; }
        public string BlendTextureName { get; set; } // Lenght 2364

        public Material()
        {
            
        }

    }
}
