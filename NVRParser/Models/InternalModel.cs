﻿namespace NvrParser.Models
{
    public class InternalModel
    {
        public int VertexIndex { get; set; }
        public int VertexOffset { get; set; }
        public int VertexLenght{ get; set; }
        public int IndiceIndex { get; set; }
        public int IndiceOffset { get; set; }
        public int IndiceLenght { get; set; }
    }
}
