﻿namespace NvrParser.Models
{
    public class Model
    {
        public float Flag1 { get; set; }
        public float Flag2 { get; set; }
        public float[] B { get; set; } // size 10

        public int Material { get; set; }
        public InternalModel model1 { get; set; }
        public InternalModel model2 { get; set; }


        public string getBReadable()
        {
            string result = "";
            result += B[0] + " " + B[1] + " " + B[2] + " " + B[3] + " " + B[4] + " " + B[5] + " " + B[6] + " " + B[7] + " " + B[8] + " " + B[9];
            return result;
        }
        public Model()
        {
            model1 = new InternalModel();
            model2 = new InternalModel();
        }
    }
}
