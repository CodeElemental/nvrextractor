﻿using System.Collections.Generic;

namespace NvrParser.Models
{
    public class VerticeList
    {
        public List<Vertice> Vertices { get; set; }

        public VerticeList()
        {
            Vertices = new List<Vertice>();
        }

        public void AddVertice(Vertice v)
        {
            Vertices.Add(v);
        }
    }
}
