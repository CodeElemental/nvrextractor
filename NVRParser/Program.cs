﻿using System;

namespace NvrParser
{
    class Program
    {
        private static void Main(string[] args)
        {
            NvrExtractor nvrProcessor = new NvrExtractor();

            if (args.Length == 0)
            {
                PrintApplicationName();
                Console.WriteLine("For a full command list use -? flag.");
            }
            else
            {
                PrintApplicationName();

                foreach (string arg in args)
                {
                    if (arg == "-?")
                    {
                        PrintUsage();
                    }

                    if (arg.StartsWith("-f:"))
                    {
                        nvrProcessor.InputFileName = arg.Substring(3);
                    }

                    if (arg.StartsWith("-o:"))
                    {
                        nvrProcessor.ModelOutputDir = arg.Substring(3);
                    }

                    if (arg.StartsWith("-avw:"))
                    {
                        nvrProcessor.AssetViewerPath = arg.Substring(5);
                        
                    }
                }
            }

            nvrProcessor.ProcessFile();
        }


        private static void PrintUsage()
        {
            Console.WriteLine("LolNvrParser usage.");
            Console.WriteLine(
                "-f:<nvr-input-filepath>                     The path of the .nvr file.");
            Console.WriteLine(
                "[-o:<nvr-output-dir>]                       (Optional) The directory to which the output will be placed.");
            Console.WriteLine(
                "[-avw:<asset-viewer-path>]                  (Optional) The path to the command line executable of Assimp© ( http://assimp.sourceforge.net/main_viewer.html ). Embedded version v3.0 ");
        }

        private static void PrintApplicationName()
        {
            Console.WriteLine("LolNvrParser© by CodinRonin.");
        }

    }
}
