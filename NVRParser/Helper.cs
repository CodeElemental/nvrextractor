﻿using System;
using System.IO;

namespace NvrParser
{
    /// <summary>
    /// Container class for Utility methods.
    /// </summary>
    public class Helper
    {
        /// <summary>
        /// Method Gets the appropriate embedded version of the AssetViewer.
        /// </summary>
        /// <returns>the path of the embedded asset viewer</returns>
        public static string GetEmbeddedAssetViewerPath()
        {
            if (IntPtr.Size == 4)
            {
                return Path.Combine(Environment.CurrentDirectory, @"Externals\AssetViewer\x86\assimp.exe");
            }
            else if (IntPtr.Size == 8)
            {
                return Path.Combine(Environment.CurrentDirectory, @"Externals\AssetViewer\x64\assimp.exe");
            }
            else
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// Method correct the directory path with a trailing "/"
        /// </summary>
        /// <param name="directory">Directory path to check.</param>
        /// <returns>Directory path with a closing "/"</returns>
        public static string SpellcheckDirectory(string directory)
        {
            if (!directory.EndsWith(@"\"))
                return directory + @"\";

            return directory;
        }
    }
}
