﻿using System;
using System.Collections.Generic;
using System.IO;
using LolNVRParser.Models;
using System.Text;

namespace LolNVRParser
{
    /// <summary>
    /// The main class containing all the parsing logic and models.
    /// </summary>
    public class FileProcessor
    {
        #region Fields / Properties
        // File names.
        public string MaterialsOutputFileName { get; set; }
        public string VerticesOutputFileName { get; set; }
        public string InputFileName { get; set; }
        public string ModelsOutputFileName { get; set; }
        public string IndicesOutputFileName { get; set; }
        public string DebugLogFile { get; set; }
        public string ModelOutputDir { get; set; }


        // the byte blob of the .nvr file.
        private byte[] fileBytes;

        // variable sizes
        private const int floatSize = 4;
        private const int intSize = 4; 

        // list of all the classes.
        private List<Material> materials;
        private List<VerticeList> verticeLists;
        private List<IndexList> indexesLists;
        private List<Model> modelsList;
        

        // current processing index in the byte array.
        private int processingIndex = 0;

        // maximum  lenght of the vertices / indices.
        private int[] maxLenght;
        private int[] maxLenghtIndex;

        // total count of elements
        private uint materialCount;
        private uint vertexListCount;
        private uint indexesListCount;
        private uint modelsCount;
        private uint unknownCount;

        // sizes of constructs
        private int materialBlockSize = 2988;
        private int verticeBaseSize = 12;
        private int verticeWithUVNormalsSize = 36;
        private int VerticeWithUVNormalsUnknownSize = 40;
        private int VerticeWithUVNormalsUnknownExtraSize = 44;
        private int unknownSize = 40;
        private int modelSize = 100;

        // start index of the vertices byte blob.
        private int verticesStartIndex;

        public bool HumanReadableOutput = true;
        #endregion

        private string header;
        public FileProcessor()
        {
            materials = new List<Material>();
            verticeLists = new List<VerticeList>();
            modelsList = new List<Model>();
            indexesLists = new List<IndexList>();
        }

        #region Processing Methods (Loops)
        public void ProcessFile(string nvrFilePath)
        {
            header = WriteHeader();
            fileBytes = File.ReadAllBytes(nvrFilePath);
            ProcessGeneralData();

            maxLenght = new int[vertexListCount];
            maxLenghtIndex = new int[indexesListCount];

            ProcessMaterials();
            PrintMaterials();

            ProcessModels();
            PrintModels();
            ExtractListMaxLenght();

            ProcessVerticeLists();
            PrintVertices();

            int sumOfIndices = 0;
            for (int i = 0; i < indexesListCount; i++)
            {
                sumOfIndices += maxLenghtIndex[i];
            }

            ProcessIndexLists();
            PrintIndices();
            PrintMaxLenght();

            GenerateDirect3dFiles();

            Console.WriteLine("Finished... press any key.");
            Console.ReadLine();
        }

        private void GenerateDirect3dFiles()
        {
            for (int i = 0; i < modelsList.Count; i++)
            {
                WriteModelFile(modelsList[i], i);
            }
        }

        private void ProcessGeneralData()
        {
            materialCount = BitConverter.ToUInt32(fileBytes, 8);
            vertexListCount = BitConverter.ToUInt32(fileBytes, 12);
            indexesListCount = BitConverter.ToUInt32(fileBytes, 16);
            modelsCount = BitConverter.ToUInt32(fileBytes, 20);
            unknownCount = BitConverter.ToUInt32(fileBytes, 24);
        }

        private void ProcessMaterials()
        {
            processingIndex = 28; // Hardcoded header offset.

            for (int i = 0; i < materialCount; i++)
            {
                Material material = ExtractMaterial();

                processingIndex += materialBlockSize;
                materials.Add(material);
            }

            verticesStartIndex = processingIndex;
        }

        private void ProcessVerticeLists()
        {
            processingIndex = verticesStartIndex;
            Console.WriteLine("Processing index for vertices =" + processingIndex);

            // The first 85 lists are all consisted of verticetype1.
            for (int i = 0; i < vertexListCount; i++)
            {
                VerticeList verticeList = new VerticeList();

                int size = BitConverter.ToInt32(fileBytes, processingIndex);

                processingIndex += intSize;

                if (size == maxLenght[i] * verticeBaseSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeBase.");

                    for (int j = 0; j < maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractBaseVertice();
                        processingIndex += verticeBaseSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == maxLenght[i] * verticeWithUVNormalsSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormals.");

                    for (int j = 0; j < maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUVNormals();
                        processingIndex += verticeWithUVNormalsSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == maxLenght[i] * VerticeWithUVNormalsUnknownSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormalsUnknown.");

                    for (int j = 0; j < maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUVNormalsUnknown();
                        processingIndex += VerticeWithUVNormalsUnknownSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == maxLenght[i] * VerticeWithUVNormalsUnknownExtraSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormalsUnknownExtra.");

                    for (int j = 0; j < maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUVNormalsUnknownExtra();
                        processingIndex += VerticeWithUVNormalsUnknownExtraSize;

                        verticeList.AddVertice(vertice);
                    }
                }

                verticeLists.Add(verticeList);
            }

            Console.WriteLine("END Processing index for vertices =" + processingIndex);
        }

        private void ProcessIndexLists()
        {
            Console.WriteLine("Processing index =" + processingIndex);

            for (int i = 0; i < indexesListCount; i++)
            {

                IndexList indexList = new IndexList();

                int size = BitConverter.ToInt32(fileBytes, processingIndex);

                indexList.Size = size;

                processingIndex += 4;

                indexList.D3DFormat = BitConverter.ToUInt32(fileBytes, processingIndex);

                processingIndex += 4;

                int numberOfElements = (int) (size/2);
                for (int j = 0;j < numberOfElements; j++)
                {
                    indexList.AddIndex(BitConverter.ToUInt16(fileBytes, processingIndex));
                    processingIndex += 2;
                }

                indexesLists.Add(indexList); 
            }
        }

        private void ProcessModels()
        {
            processingIndex = fileBytes.Length - (int)(unknownCount * unknownSize) - (int)(modelSize * modelsCount);
            Console.WriteLine("Processing index for models =" + processingIndex);

            for (int i = 0; i < modelsCount; i++)
            {
                Model model = ExtractModel();

                processingIndex += modelSize;
                modelsList.Add(model);
            }

            Console.WriteLine("END Processing index for vertices =" + processingIndex);
        }
        #endregion

        #region Extraction Methods

        private void ExtractListMaxLenght()
        {
            foreach (Model model in modelsList)
            {
                // extract vertex max lenght from model.
                if (model.model1.VertexIndex < 0)
                {
                    model.model1.VertexIndex = 0;
                }
                if (model.model2.VertexIndex < 0)
                {
                    model.model2.VertexIndex = 0;
                }

                int maxToCompare = maxLenght[model.model1.VertexIndex];
                int max = model.model1.VertexOffset + model.model1.VertexLenght;

                if (maxToCompare < max)
                {
                    maxLenght[model.model1.VertexIndex] = max;
                }

                int maxToCompare2 = maxLenght[model.model2.VertexIndex];
                int max2 = model.model2.VertexOffset + model.model2.VertexLenght;
                if (maxToCompare2 < max2)
                {
                    maxLenght[model.model2.VertexIndex] = max2;
                }

                // extract index max lenght from model
                if (model.model1.IndiceIndex < 0)
                {
                    model.model1.IndiceIndex = 0;
                }
                if (model.model2.IndiceIndex < 0)
                {
                    model.model2.IndiceIndex = 0;
                }

                int maxIToCompare = maxLenghtIndex[model.model1.IndiceIndex];
                int maxI = model.model1.IndiceOffset + model.model1.IndiceLenght;

                if (maxIToCompare < maxI)
                {
                    maxLenghtIndex[model.model1.IndiceIndex] = maxI;
                }

                int maxIToCompare2 = maxLenghtIndex[model.model2.IndiceIndex];
                int maxI2 = model.model2.IndiceOffset + model.model2.IndiceLenght;
                if (maxIToCompare2 < maxI2)
                {
                    maxLenghtIndex[model.model2.IndiceIndex] = maxI2;
                }
            }
        }

        private Material ExtractMaterial()
        {
            Material material = new Material();
            int index = 0;
            material.MaterialName = ReadAsString(processingIndex + index, 256);
            index += 256;

            material.EmissiveColor = ExtractInts(processingIndex + index, 3);
            index += (3 * 4);

            material.BlendColor = ExtractFloats(processingIndex + index, 4);
            index += (4 * floatSize);

            material.TextureFileName = ReadAsString(processingIndex + index, 336);
            index += 336;

            material.Opacity = BitConverter.ToSingle(fileBytes, processingIndex + index);
            index += floatSize;

            material.BlendTextureName = ReadAsString(processingIndex + index, 336);

            return material;
        }

        private Model ExtractModel()
        {
            Model model = new Model();
            int index = 0;

            model.Flag1 = BitConverter.ToUInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.Flag2 = BitConverter.ToUInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.B = ExtractFloats(processingIndex + index, 10);
            index += 10*floatSize;

            model.Material = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.VertexIndex = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.VertexOffset = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.VertexLenght = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.IndiceIndex = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.IndiceOffset = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model1.IndiceLenght = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.VertexIndex = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.VertexOffset = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.VertexLenght = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.IndiceIndex = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.IndiceOffset = BitConverter.ToInt32(fileBytes, processingIndex + index);
            index += intSize;

            model.model2.IndiceLenght = BitConverter.ToInt32(fileBytes, processingIndex + index);

            return model;
        }

        private Vertice ExtractBaseVertice()
        {
            Vertice vertice = new Vertice();

            vertice.Position = ExtractFloats(processingIndex, 3);

            return vertice;
        }

        private Vertice ExtractVerticeWithUVNormals()
        {
            VerticeWithUVNormals vertice = new VerticeWithUVNormals();
            int index = 0;

            vertice.Position = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.Normals = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.UV = ExtractFloats(processingIndex + index, 2);

            return vertice;
        }

        private Vertice ExtractVerticeWithUVNormalsUnknown()
        {
            VerticeWithUVNormalsUnknown vertice = new VerticeWithUVNormalsUnknown();
            int index = 0;

            vertice.Position = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.Normals = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.UV = ExtractFloats(processingIndex + index, 2);
            index += (2 * floatSize);

            vertice.Unknown = ExtractBytes(processingIndex + index, 4);

            return vertice;
        }

        private Vertice ExtractVerticeWithUVNormalsUnknownExtra()
        {
            VerticeWithUVNormalsUnknownExtra vertice = new VerticeWithUVNormalsUnknownExtra();
            int index = 0;

            vertice.Position = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.Normals = ExtractFloats(processingIndex + index, 3);
            index += (3 * floatSize);

            vertice.Extra = ExtractBytes(processingIndex + index, 4);
            index += 4;

            vertice.Unknown = ExtractBytes(processingIndex + index, 4);
            index += 4;

            vertice.UV = ExtractFloats(processingIndex + index, 2);

            return vertice;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Extracts provided number of floats from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="count">The number of floats to extract.</param>
        /// <returns>the float array of extracted floats.</returns>
        private float[] ExtractFloats(int offset, int count)
        {
            float[] result = new float[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = (float) Math.Round(BitConverter.ToSingle(fileBytes, offset + i * floatSize), 3);
            }
            return result;
        }

        /// <summary>
        /// Extracts provided number of ints from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="count">The number of ints to extract.</param>
        /// <returns>the float array of extracted ints.</returns>
        private int[] ExtractInts(int offset, int lenght)
        {
            int[] result = new int[lenght];
            for (int i = 0; i < lenght; i++)
            {
                result[i] = BitConverter.ToInt32(fileBytes, offset + i * 4);
            }
            return result;
        }

        /// <summary>
        /// Extracts provided number of bytes from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="count">The number of bytes to extract.</param>
        /// <returns>the float array of extracted bytes.</returns>
        private byte[] ExtractBytes(int offset, int lenght)
        {
            byte[] result = new byte[lenght];
            for (int i = 0; i < lenght; i++)
            {
                result[i] = fileBytes[offset + i];
            }
            return result;
        }


        /// <summary>
        /// Extracts string from the byte blob, starting from provided offset with provided lenght. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="count">The number of bytes to extract.</param>
        /// <returns>the string.</returns>
        private string ReadAsString(int offset, int lenght)
        {
            string result = "";
            for (int i = 0; i < lenght; i++)
            {
                if (fileBytes[offset + i] != 0)
                {
                    result += (char)fileBytes[offset + i];
                }
                else
                {
                    break;
                }
               
            }

            return result;
        }

        #endregion

        #region Printing methods

        private void PrintIndices()
        {
            using (StreamWriter file = new StreamWriter(IndicesOutputFileName))
            {
                for (int i = 0; i < indexesLists.Count; i++)
                {

                    for (int j = 0 ; j < indexesLists[i].Indexes.Count; j++)
                    {
                        if (HumanReadableOutput)
                        {
                            file.WriteLine(i + "-" + j + " : " + indexesLists[i].Indexes[j]);
                        }
                        else
                        {
                            file.Write(indexesLists[i].Indexes[j]);
                            if (j < indexesLists[i].Indexes.Count - 1)
                                file.Write(",");
                        }
                    }

                    if (i < indexesLists.Count - 1)
                        file.Write("|");
                }
                file.Close();
            }
        }

        private void PrintMaterials()
        {
            using (StreamWriter file = new StreamWriter(MaterialsOutputFileName))
            {
                for (int i = 0; i < materials.Count ; i++)
                {
                    Material material = materials[i];

                    if (HumanReadableOutput)
                    {
                        file.WriteLine("[" + material.MaterialName + "]");
                        file.WriteLine("Emissing color : " + material.EmissiveColor[0] + " " + material.EmissiveColor[1] +
                                       " " + material.EmissiveColor[2]);
                        file.WriteLine("Blending color : " + material.BlendColor[0] + " " + material.BlendColor[1] + " " +
                                       material.BlendColor[2] + " " + material.BlendColor[3]);
                        file.WriteLine("TextureFileName : " + material.TextureFileName);
                        file.WriteLine("Opacity : " + material.Opacity);
                        file.WriteLine("BlendTextureName : " + material.BlendTextureName);
                        file.WriteLine("");
                    }
                    else
                    {
                        file.Write(material.TextureFileName);

                        if (i < materials.Count - 1)
                            file.Write(",");
                    }
                }
                file.Close();
            }
        }

        private void PrintModels()
        {
            using (StreamWriter file = new StreamWriter(ModelsOutputFileName))
            {
                for (int i = 0; i < modelsList.Count; i++ )
                {
                    if (HumanReadableOutput)
                    {
                        file.WriteLine("[ Model number " + i + " ]");
                        file.WriteLine("Fl0=" + modelsList[i].Flag1 + " Fl1=" + modelsList[i].Flag2);
                        file.WriteLine("B : " + modelsList[i].getBReadable() + " ");
                        file.WriteLine("MaterialIndex : " + modelsList[i].Material + " ( " +
                                       (modelsList[i].Material < materials.Count
                                            ? materials[modelsList[i].Material].MaterialName
                                            : "-Cannot be found-") + " )");
                        file.WriteLine("Internal Model 1 : VI:" + modelsList[i].model1.VertexIndex + " VO:" +
                                       modelsList[i].model1.VertexOffset + " VL:" + modelsList[i].model1.VertexLenght +
                                       " II:" + modelsList[i].model1.IndiceIndex + " IO:" +
                                       modelsList[i].model1.IndiceOffset + " IL:" + modelsList[i].model1.IndiceLenght);
                        file.WriteLine("Internal Model 2 : VI:" + modelsList[i].model2.VertexIndex + " VO:" +
                                       modelsList[i].model2.VertexOffset + " VL:" + modelsList[i].model2.VertexLenght +
                                       " II:" + modelsList[i].model2.IndiceIndex + " IO:" +
                                       modelsList[i].model2.IndiceOffset + " IL:" + modelsList[i].model2.IndiceLenght);
                        file.WriteLine("");
                    }
                    else
                    {
                        string line = modelsList[i].Material + "!";
                        line += modelsList[i].model1.VertexIndex + ","
                               + modelsList[i].model1.VertexOffset + ","
                               + modelsList[i].model1.VertexLenght + ":"
                               + modelsList[i].model1.IndiceIndex + ","
                               + modelsList[i].model1.IndiceOffset + ","
                               + modelsList[i].model1.IndiceLenght + ";"
                               + modelsList[i].model2.VertexIndex + ","
                               + modelsList[i].model2.VertexOffset + ","
                               + modelsList[i].model2.VertexLenght + ":"
                               + modelsList[i].model2.IndiceIndex + ","
                               + modelsList[i].model2.IndiceOffset + ","
                               + modelsList[i].model2.IndiceLenght;
                        if (i < modelsList.Count - 1)
                            line += "|";

                        file.Write(line);
                    }

                }
               file.Close();
            }
        }

        private void PrintMaxLenght()
        {
            using (StreamWriter file = new StreamWriter(DebugLogFile))
            {
                for (int i = 0; i < vertexListCount; i++)
                {
                    file.WriteLine("[ Max index for list number " + i + " is " + maxLenght[i] + "]");
                }
                file.Close();
            }
        }

        private void PrintVertices()
        {
            using (StreamWriter file = new StreamWriter(VerticesOutputFileName))
            {
                for (int i = 0; i < verticeLists.Count; i++)
                {
                    VerticeList verticeList = verticeLists[i];
                    for (int j = 0; j < verticeList.Vertices.Count; j++)
                    {
                        if (HumanReadableOutput)
                        {

                            file.WriteLine(i + "-" + j + " : ");
                            file.WriteLine("[" + "Position : " + verticeList.Vertices[j].Position[0] + " " +
                                           verticeList.Vertices[j].Position[1] + " " +
                                           verticeList.Vertices[j].Position[2] + " ]");
                            if (verticeList.Vertices[j] is VerticeWithUVNormals)
                            {
                                VerticeWithUVNormals vertice = verticeList.Vertices[j] as VerticeWithUVNormals;
                                file.WriteLine("[" + "Normals : " + vertice.Normals[0] + " " + vertice.Normals[1] + " " +
                                               vertice.Normals[2] + " ]");
                                file.WriteLine("[" + "UV : " + vertice.UV[0] + " " + vertice.UV[1] + " ]");
                            }
                            if (verticeList.Vertices[j] is VerticeWithUVNormalsUnknown)
                            {
                                VerticeWithUVNormalsUnknown v2 =
                                    (verticeList.Vertices[j] as VerticeWithUVNormalsUnknown);

                                file.WriteLine("[" + "Unknown : " + v2.getUnknownReadable() + " ]");
                            }
                            if (verticeList.Vertices[j] is VerticeWithUVNormalsUnknownExtra)
                            {
                                VerticeWithUVNormalsUnknownExtra v2 =
                                    (verticeList.Vertices[j] as VerticeWithUVNormalsUnknownExtra);

                                file.WriteLine("[" + "Extra : " + v2.getReadableExtra() + " ]");
                            }
                        }
                        else
                        {
                            string UV = "";
                            string Norm = "";
                            if (verticeList.Vertices[j] is VerticeWithUVNormals)
                            {
                                VerticeWithUVNormals vertice = verticeList.Vertices[j] as VerticeWithUVNormals;
                                UV = "UV:" + Math.Round(vertice.UV[0]) + "!" + Math.Round(vertice.UV[1]);
                                Norm = "N:" + Math.Round(vertice.Normals[0]) + "!" + Math.Round(vertice.Normals[1])
                                       + "!" + Math.Round(vertice.Normals[2]);
                            }


                            string line =
                                "P:" + Math.Round(verticeList.Vertices[j].Position[0], 4)
                                + "!" + Math.Round(verticeList.Vertices[j].Position[1])
                                + "!" + Math.Round(verticeList.Vertices[j].Position[2])
                                + (string.IsNullOrEmpty(UV) ? "" : "_" + UV)
                                + (string.IsNullOrEmpty(Norm) ? "" : "_" + Norm);

                            if (j < verticeList.Vertices.Count - 1)
                            {
                                line += ";";
                            }

                            file.Write(line);
                        }
                    }

                    if (i < verticeLists.Count - 1)
                        file.Write("|");

                }
                file.Close();
            }
        }
        #endregion



        private void WriteToFile(string filename)
        {
            
        }

        private string WriteHeader()
        {
            string ret = "xof 0303txt 0032\n" +
                "template Vector {\n" +
                " <3d82ab5e-62da-11cf-ab39-0020af71e433>\n" +
                " FLOAT x;\n" +
                " FLOAT y;\n" +
                " FLOAT z;\n" +
                "}\n" +
                "\n" +
                "template MeshFace {\n" +
                " <3d82ab5f-62da-11cf-ab39-0020af71e433>\n" +
                " DWORD nFaceVertexIndices;\n" +
                " array DWORD faceVertexIndices[nFaceVertexIndices];\n" +
                "}\n" +
                "\n" +
                "template Mesh {\n" +
                " <3d82ab44-62da-11cf-ab39-0020af71e433>\n" +
                " DWORD nVertices;\n" +
                " array Vector vertices[nVertices];\n" +
                " DWORD nFaces;\n" +
                " array MeshFace faces[nFaces];\n" +
                " [...]\n" +
                "}\n" +
                "\n" +
                "template MeshNormals {\n" +
                " <f6f23f43-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nNormals;\n" +
                " array Vector normals[nNormals];\n" +
                " DWORD nFaceNormals;\n" +
                " array MeshFace faceNormals[nFaceNormals];\n" +
                "}\n" +
                "\n" +
                "template Coords2d {\n" +
                " <f6f23f44-7686-11cf-8f52-0040333594a3>\n" +
                " FLOAT u;\n" +
                " FLOAT v;\n" +
                "}\n" +
                "\n" +
                "template MeshTextureCoords {\n" +
                " <f6f23f40-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nTextureCoords;\n" +
                " array Coords2d textureCoords[nTextureCoords];\n" +
                "}\n" +
                "\n" +
                "template Material {\n" +
                " <3d82ab4d-62da-11cf-ab39-0020af71e433>\n" +
                " ColorRGBA faceColor;\n" +
                " FLOAT power;\n" +
                " ColorRGB specularColor;\n" +
                " ColorRGB emissiveColor;\n" +
                " [...]\n" +
                "}\n" +
                "\n" +
                "template MeshMaterialList {\n" +
                " <f6f23f42-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nMaterials;\n" +
                " DWORD nFaceIndexes;\n" +
                " array DWORD faceIndexes[nFaceIndexes];\n" +
                " [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]\n" +
                "}\n" +
                "\n" +
                "template TextureFilename {\n" +
                " <a42790e1-7810-11cf-8f52-0040333594a3>\n" +
                " STRING filename;\n" +
                "}\n";
            return ret;
        }

        

        private void WriteModelFile(Models.Model model, int number)
        {

            int XFile_vertex_index = model.model1.VertexIndex;
            int XFile_vertex_offset = model.model1.VertexOffset;
            int XFile_vertex_length = model.model1.VertexLenght;

            int XFile_index_index = model.model1.IndiceIndex;
            int XFile_index_offset = model.model1.IndiceOffset;
            int XFile_index_length = model.model1.IndiceLenght;
            

            string filename = ModelOutputDir + materials[model.Material].MaterialName + number + ".x";

            using (StreamWriter file = new StreamWriter(filename))
            {

                file.Write(header);
                file.Write("Mesh {\n");
                file.Write(" " + XFile_vertex_length + ";\n");

                // Vertices
                for (int j = XFile_vertex_offset; j < XFile_vertex_offset + XFile_vertex_length; j++)
                {
                    if (j < XFile_vertex_offset + XFile_vertex_length - 1)
                    {
                        file.Write(" " + verticeLists[XFile_vertex_index].Vertices[j].Position[0] + ";");
                        file.Write(verticeLists[XFile_vertex_index].Vertices[j].Position[1] + ";");
                        file.Write(verticeLists[XFile_vertex_index].Vertices[j].Position[2] + ";,\n");
                    }
                    else
                    {
                        file.Write(" " + verticeLists[XFile_vertex_index].Vertices[j].Position[0] + ";");
                        file.Write(verticeLists[XFile_vertex_index].Vertices[j].Position[1] + ";");
                        file.Write(verticeLists[XFile_vertex_index].Vertices[j].Position[2] + ";;\n\n");
                    }
                }

                // Indices
                file.Write(" " + ((int)(XFile_index_length / 3)) + ";\n");
                for (int j = XFile_index_offset; j < XFile_index_offset + XFile_index_length; j += 3)
                {
                    if (j < XFile_index_offset + XFile_index_length - 3)
                    {
                        file.Write(" 3;" + (indexesLists[XFile_index_index].Indexes[j] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 1] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 2] - XFile_vertex_offset) + ";,\n");
                    }
                    else
                    {
                        file.Write(" 3;" + (indexesLists[XFile_index_index].Indexes[j] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 1] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 2] - XFile_vertex_offset) + ";;\n\n");
                    }
                }

                // Normals
                file.Write(" MeshNormals {\n  " + XFile_vertex_length + ";\n");
                for (int j = XFile_vertex_offset; j < XFile_vertex_offset + XFile_vertex_length; j++)
                {

                    if (verticeLists[XFile_vertex_index].Vertices[j] is VerticeWithUVNormals)
                    {
                        VerticeWithUVNormals vert = verticeLists[XFile_vertex_index].Vertices[j] as VerticeWithUVNormals;
                        if (vert != null)
                        {
                            if (j < XFile_vertex_offset + XFile_vertex_length - 1)
                            {
                                file.Write("  " + vert.Normals[0] + ";");
                                file.Write(vert.Normals[1] + ";");
                                file.Write(vert.Normals[2] + ";,\n");
                            }
                            else
                            {
                                file.Write("  " + vert.Normals[0] + ";");
                                file.Write(vert.Normals[1] + ";");
                                file.Write(vert.Normals[2] + ";;\n\n");
                            }
                        }
                    }

                }


                // Normals indices
                file.Write("  " + ((int)XFile_index_length / 3) + ";\n");
                for (int j = XFile_index_offset; j < XFile_index_offset + XFile_index_length; j += 3)
                {
                    if (j < XFile_index_offset + XFile_index_length - 3)
                    {
                        file.Write("  3;" + (indexesLists[XFile_index_index].Indexes[j] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 1] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 2] - XFile_vertex_offset) + ";,\n");
                    }
                    else
                    {
                        file.Write("  3;" + (indexesLists[XFile_index_index].Indexes[j] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 1] - XFile_vertex_offset) + ",");
                        file.Write((indexesLists[XFile_index_index].Indexes[j + 2] - XFile_vertex_offset) + ";;\n }\n\n");
                    }
                }

                //texture coodrinates
                file.Write(" MeshTextureCoords {\n  " + XFile_vertex_length + ";\n");
                for (int j = XFile_vertex_offset; j < XFile_vertex_offset + XFile_vertex_length; j++)
                {
                    if (verticeLists[XFile_vertex_index].Vertices[j] is VerticeWithUVNormals)
                    {
                        VerticeWithUVNormals vert = verticeLists[XFile_vertex_index].Vertices[j] as VerticeWithUVNormals;
                        if (vert != null)
                        {
                            if (j < XFile_vertex_offset + XFile_vertex_length - 1)
                            {
                                file.Write("  " + vert.UV[0] + ";");
                                file.Write(vert.UV[1] + ";,\n");
                            }
                            else
                            {
                                file.Write("  " + vert.UV[0] + ";");
                                file.Write(vert.UV[1] + ";;\n }\n\n");
                            }
                        }
                        
                    }
                }

                //material list
                file.Write(" MeshMaterialList {\n  1;\n  1;\n  0;\n\n");
                //material
                file.Write("  Material {\n");
                file.Write("   1.000000;1.000000;1.000000;1.000000;;\n");
                file.Write("   3.200000;\n");
                file.Write("   0.000000;0.000000;0.000000;;\n");
                file.Write("   0.000000;0.000000;0.000000;;\n\n");
                //texutre
                file.Write("   TextureFilename {\n");
                file.Write("    \"texture\\\\" + materials[model.Material].TextureFileName + "\";\n   }\n  }\n }\n}");
            }
        }
    }
}
