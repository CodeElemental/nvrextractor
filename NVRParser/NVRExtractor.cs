﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using NvrParser.Models;

namespace NvrParser
{
    /// <summary>
    /// The main class containing all the processing logic.
    /// </summary>
    public class NvrExtractor
    {
        #region Fields / Properties

        // File names.
        public string InputFileName { get; set; }
        public string ModelOutputDir { get; set; }
        public string AssetViewerPath { get; set; }
        private string _objOutputDir;

        // the byte blob of the .nvr file.
        private byte[] _fileBytes;

        // variable sizes
        private const int FloatSize = 4;
        private const int IntSize = 4; 

        // list of all the data to be extracted.
        private readonly List<Material> _materials;
        private readonly List<VerticeList> _verticeLists;
        private readonly List<IndexList> _indexesLists;
        private readonly List<Model> _modelsList;
        
        // current processing index in the byte array.
        private int _processingIndex;

        // maximum  lenght of the vertices / indices.
        private int[] _maxLenght;
        private int[] _maxLenghtIndex;

        // total count of elements
        private uint _materialCount;
        private uint _vertexListCount;
        private uint _indexesListCount;
        private uint _modelsCount;
        private uint _unknownCount;

        // sizes of constructs
        private const int MaterialBlockSize = 2988;
        private const int VerticeBaseSize = 12;
        private const int VerticeWithUvNormalsSize = 36;
        private const int VerticeWithUvNormalsUnknownSize = 40;
        private const int VerticeWithUvNormalsUnknownExtraSize = 44;
        private const int UnknownSize = 40;
        private const int ModelSize = 100;

        // start index of the vertices byte blob.
        private int _verticesStartIndex;

        public bool HumanReadableOutput = true;
        #endregion

        private string _header;

        /// <summary>
        /// Constructor
        /// </summary>
        public NvrExtractor()
        {
            _materials = new List<Material>();
            _verticeLists = new List<VerticeList>();
            _modelsList = new List<Model>();
            _indexesLists = new List<IndexList>();
        }

        #region Processing Methods (Loops)

        /// <summary>
        /// Method that initiates the conversion. 
        /// </summary>
        /// <remarks>All the initialization needs to be done before calling this method.</remarks>
        public void ProcessFile()
        {
            if (!ValidateParameters())
                return;

            _objOutputDir = ModelOutputDir + @"Obj\";

            Console.WriteLine("InputFileName : " + InputFileName);
            Console.WriteLine("ModelOutputDir : " + ModelOutputDir);
            Console.WriteLine("assetViewr : " + AssetViewerPath);

            // Extract the nvr data.
            _fileBytes = File.ReadAllBytes(InputFileName);
            ProcessGeneralData();

            _maxLenght = new int[_vertexListCount];
            _maxLenghtIndex = new int[_indexesListCount];

            ProcessMaterials();
            ProcessModels();
            ExtractListMaxLenght();
            ProcessVerticeLists();
            ProcessIndexLists();


            // Process the extracted data.
            PrintMaterials();
            _header = WriteHeader();
            GenerateDirect3DFiles();
            CreateAndExecuteAssetViewerBatch();

            Console.WriteLine("Finished... press any key.");
            Console.ReadLine();
        }

        /// <summary>
        /// Method initializes the number of elements for each structure.
        /// </summary>
        private void ProcessGeneralData()
        {
            _materialCount = BitConverter.ToUInt32(_fileBytes, 8);
            _vertexListCount = BitConverter.ToUInt32(_fileBytes, 12);
            _indexesListCount = BitConverter.ToUInt32(_fileBytes, 16);
            _modelsCount = BitConverter.ToUInt32(_fileBytes, 20);
            _unknownCount = BitConverter.ToUInt32(_fileBytes, 24);
        }

        /// <summary>
        /// Method processes the materials.
        /// </summary>
        private void ProcessMaterials()
        {
            _processingIndex = 28; // Hardcoded header offset.

            for (int i = 0; i < _materialCount; i++)
            {
                Material material = ExtractMaterial();

                _processingIndex += MaterialBlockSize;
                _materials.Add(material);
            }

            _verticesStartIndex = _processingIndex;
        }

        /// <summary>
        /// Method processes the vertices.
        /// </summary>
        private void ProcessVerticeLists()
        {
            _processingIndex = _verticesStartIndex;
            Console.WriteLine("Processing index for vertices =" + _processingIndex);

            for (int i = 0; i < _vertexListCount; i++)
            {
                VerticeList verticeList = new VerticeList();

                int size = BitConverter.ToInt32(_fileBytes, _processingIndex);

                _processingIndex += IntSize;

                if (size == _maxLenght[i] * VerticeBaseSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeBase.");

                    for (int j = 0; j < _maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractBaseVertice();
                        _processingIndex += VerticeBaseSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == _maxLenght[i] * VerticeWithUvNormalsSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormals.");

                    for (int j = 0; j < _maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUvNormals();
                        _processingIndex += VerticeWithUvNormalsSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == _maxLenght[i] * VerticeWithUvNormalsUnknownSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormalsUnknown.");

                    for (int j = 0; j < _maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUvNormalsUnknown();
                        _processingIndex += VerticeWithUvNormalsUnknownSize;

                        verticeList.AddVertice(vertice);
                    }
                }
                else if (size == _maxLenght[i] * VerticeWithUvNormalsUnknownExtraSize)
                {
                    Console.WriteLine("  Size of list  " + i + " in bytes =" + size + " Type of list : VerticeWithUVNormalsUnknownExtra.");

                    for (int j = 0; j < _maxLenght[i]; j++)
                    {
                        Vertice vertice = ExtractVerticeWithUvNormalsUnknownExtra();
                        _processingIndex += VerticeWithUvNormalsUnknownExtraSize;

                        verticeList.AddVertice(vertice);
                    }
                }

                _verticeLists.Add(verticeList);
            }

            Console.WriteLine("END Processing index for vertices =" + _processingIndex);
        }

        /// <summary>
        /// Method processes the Indexes.
        /// </summary>
        private void ProcessIndexLists()
        {
            Console.WriteLine("Processing index =" + _processingIndex);

            for (int i = 0; i < _indexesListCount; i++)
            {

                IndexList indexList = new IndexList();

                int size = BitConverter.ToInt32(_fileBytes, _processingIndex);

                indexList.Size = size;

                _processingIndex += 4;

                indexList.D3DFormat = BitConverter.ToUInt32(_fileBytes, _processingIndex);

                _processingIndex += 4;

                int numberOfElements = (size/2);
                for (int j = 0;j < numberOfElements; j++)
                {
                    indexList.AddIndex(BitConverter.ToUInt16(_fileBytes, _processingIndex));
                    _processingIndex += 2;
                }

                _indexesLists.Add(indexList); 
            }
        }


        /// <summary>
        /// Method processes the models.
        /// </summary>
        private void ProcessModels()
        {
            _processingIndex = _fileBytes.Length - (int)(_unknownCount * UnknownSize) - (int)(ModelSize * _modelsCount);
            Console.WriteLine("Processing index for models =" + _processingIndex);

            for (int i = 0; i < _modelsCount; i++)
            {
                Model model = ExtractModel();

                _processingIndex += ModelSize;
                _modelsList.Add(model);
            }

            Console.WriteLine("END Processing index for vertices =" + _processingIndex);
        }

        /// <summary>
        /// Method generates the Direct3d files (.x).
        /// </summary>
        private void GenerateDirect3DFiles()
        {
            Console.WriteLine("Generating Direct3d(.x) files");
            Console.Write("Working:");
            for (int i = 0; i < _modelsList.Count; i++)
            {
                WriteModelFile(_modelsList[i], i);
                int reminder = 0;
                Math.DivRem(i, 100, out reminder);
                if (reminder == 0)
                {
                    Console.Write(".");
                }
            }

            Console.WriteLine("");
            Console.WriteLine("Finished Generating Direct3d(.x) files.");
        }

        #endregion

        #region Extraction Methods

        /// <summary>
        /// Method extracts the maximum lenght of the vertices and indices, using the data provided in the models. 
        /// </summary>
        private void ExtractListMaxLenght()
        {
            foreach (Model model in _modelsList)
            {
                // extract vertex max lenght from model.
                if (model.model1.VertexIndex < 0)
                {
                    model.model1.VertexIndex = 0;
                }
                if (model.model2.VertexIndex < 0)
                {
                    model.model2.VertexIndex = 0;
                }

                int maxToCompare = _maxLenght[model.model1.VertexIndex];
                int max = model.model1.VertexOffset + model.model1.VertexLenght;

                if (maxToCompare < max)
                {
                    _maxLenght[model.model1.VertexIndex] = max;
                }

                int maxToCompare2 = _maxLenght[model.model2.VertexIndex];
                int max2 = model.model2.VertexOffset + model.model2.VertexLenght;
                if (maxToCompare2 < max2)
                {
                    _maxLenght[model.model2.VertexIndex] = max2;
                }

                // extract index max lenght from model
                if (model.model1.IndiceIndex < 0)
                {
                    model.model1.IndiceIndex = 0;
                }
                if (model.model2.IndiceIndex < 0)
                {
                    model.model2.IndiceIndex = 0;
                }

                int maxIToCompare = _maxLenghtIndex[model.model1.IndiceIndex];
                int maxI = model.model1.IndiceOffset + model.model1.IndiceLenght;

                if (maxIToCompare < maxI)
                {
                    _maxLenghtIndex[model.model1.IndiceIndex] = maxI;
                }

                int maxIToCompare2 = _maxLenghtIndex[model.model2.IndiceIndex];
                int maxI2 = model.model2.IndiceOffset + model.model2.IndiceLenght;
                if (maxIToCompare2 < maxI2)
                {
                    _maxLenghtIndex[model.model2.IndiceIndex] = maxI2;
                }
            }
        }


        /// <summary>
        /// Method extracts single material.
        /// </summary>
        /// <returns><seealso cref="Material"/> instance.</returns>
        private Material ExtractMaterial()
        {
            Material material = new Material();
            int index = 0;
            material.MaterialName = ReadAsString(_processingIndex + index, 256);
            index += 256;

            material.EmissiveColor = ExtractInts(_processingIndex + index, 3);
            index += (3 * 4);

            material.BlendColor = ExtractFloats(_processingIndex + index, 4);
            index += (4 * FloatSize);

            material.TextureFileName = ReadAsString(_processingIndex + index, 336);
            index += 336;

            material.Opacity = BitConverter.ToSingle(_fileBytes, _processingIndex + index);
            index += FloatSize;

            material.BlendTextureName = ReadAsString(_processingIndex + index, 336);

            return material;
        }


        /// <summary>
        /// Method extracts single model.
        /// </summary>
        /// <returns><see cref="Model"/> instance.</returns>
        private Model ExtractModel()
        {
            Model model = new Model();
            int index = 0;

            model.Flag1 = BitConverter.ToUInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.Flag2 = BitConverter.ToUInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.B = ExtractFloats(_processingIndex + index, 10);
            index += 10*FloatSize;

            model.Material = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.VertexIndex = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.VertexOffset = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.VertexLenght = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.IndiceIndex = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.IndiceOffset = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model1.IndiceLenght = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.VertexIndex = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.VertexOffset = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.VertexLenght = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.IndiceIndex = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.IndiceOffset = BitConverter.ToInt32(_fileBytes, _processingIndex + index);
            index += IntSize;

            model.model2.IndiceLenght = BitConverter.ToInt32(_fileBytes, _processingIndex + index);

            return model;
        }

        /// <summary>
        /// Method extracts a single base vertice.
        /// </summary>
        /// <returns><see cref="Vertice"/> instance.</returns>
        private Vertice ExtractBaseVertice()
        {
            Vertice vertice = new Vertice();

            vertice.Position = ExtractFloats(_processingIndex, 3);

            return vertice;
        }

        /// <summary>
        /// Method extracts single Vertice with UV and normals.
        /// </summary>
        /// <returns><see cref="VerticeWithUVNormals"/> instance.</returns>
        private Vertice ExtractVerticeWithUvNormals()
        {
            VerticeWithUVNormals vertice = new VerticeWithUVNormals();
            int index = 0;

            vertice.Position = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.Normals = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.UV = ExtractFloats(_processingIndex + index, 2);

            return vertice;
        }

        /// <summary>
        /// Method extracts a single vertice with UV , normals and unknown data.
        /// </summary>
        /// <returns><see cref="VerticeWithUVNormalsUnknown"/> instance.</returns>
        private Vertice ExtractVerticeWithUvNormalsUnknown()
        {
            VerticeWithUVNormalsUnknown vertice = new VerticeWithUVNormalsUnknown();
            int index = 0;

            vertice.Position = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.Normals = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.UV = ExtractFloats(_processingIndex + index, 2);
            index += (2 * FloatSize);

            vertice.Unknown = ExtractBytes(_processingIndex + index, 4);

            return vertice;
        }


        /// <summary>
        /// Method extracts a single vertice with UV , normals and extra unknown data.
        /// </summary>
        /// <returns><see cref="VerticeWithUVNormalsUnknownExtra"/> instance.</returns>
        private Vertice ExtractVerticeWithUvNormalsUnknownExtra()
        {
            VerticeWithUVNormalsUnknownExtra vertice = new VerticeWithUVNormalsUnknownExtra();
            int index = 0;

            vertice.Position = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.Normals = ExtractFloats(_processingIndex + index, 3);
            index += (3 * FloatSize);

            vertice.Extra = ExtractBytes(_processingIndex + index, 4);
            index += 4;

            vertice.Unknown = ExtractBytes(_processingIndex + index, 4);
            index += 4;

            vertice.UV = ExtractFloats(_processingIndex + index, 2);

            return vertice;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Extracts provided number of floats from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="count">The number of floats to extract.</param>
        /// <returns>the float array of extracted floats.</returns>
        private float[] ExtractFloats(int offset, int count)
        {
            float[] result = new float[count];
            for (int i = 0; i < count; i++)
            {
                result[i] = (float) Math.Round(BitConverter.ToSingle(_fileBytes, offset + i * FloatSize), 3);
            }
            return result;
        }

        /// <summary>
        /// Extracts provided number of ints from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="lenght">The number of ints to extract.</param>
        /// <returns>the float array of extracted ints.</returns>
        private int[] ExtractInts(int offset, int lenght)
        {
            int[] result = new int[lenght];
            for (int i = 0; i < lenght; i++)
            {
                result[i] = BitConverter.ToInt32(_fileBytes, offset + i * 4);
            }
            return result;
        }

        /// <summary>
        /// Extracts provided number of bytes from the byte blob, starting from provided offset. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="lenght">The number of bytes to extract.</param>
        /// <returns>the float array of extracted bytes.</returns>
        private byte[] ExtractBytes(int offset, int lenght)
        {
            byte[] result = new byte[lenght];
            for (int i = 0; i < lenght; i++)
            {
                result[i] = _fileBytes[offset + i];
            }
            return result;
        }


        /// <summary>
        /// Extracts string from the byte blob, starting from provided offset with provided lenght. 
        /// </summary>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="lenght">The number of bytes to extract.</param>
        /// <returns>the string.</returns>
        private string ReadAsString(int offset, int lenght)
        {
            string result = "";
            for (int i = 0; i < lenght; i++)
            {
                if (_fileBytes[offset + i] != 0)
                {
                    result += (char)_fileBytes[offset + i];
                }
                else
                {
                    break;
                }
               
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Prints the materials to the output txt file.
        /// </summary>
        private void PrintMaterials()
        {
            using (StreamWriter file = new StreamWriter(ModelOutputDir + "Materials.txt"))
            {
                for (int i = 0; i < _materials.Count ; i++)
                {
                    Material material = _materials[i];

                    if (HumanReadableOutput)
                    {
                        file.WriteLine("[" + material.MaterialName + "]");
                        file.WriteLine("Emissing color : " + material.EmissiveColor[0] + " " + material.EmissiveColor[1] +
                                       " " + material.EmissiveColor[2]);
                        file.WriteLine("Blending color : " + material.BlendColor[0] + " " + material.BlendColor[1] + " " +
                                       material.BlendColor[2] + " " + material.BlendColor[3]);
                        file.WriteLine("TextureFileName : " + material.TextureFileName);
                        file.WriteLine("Opacity : " + material.Opacity);
                        file.WriteLine("BlendTextureName : " + material.BlendTextureName);
                        file.WriteLine("");
                    }
                    else
                    {
                        file.Write(material.TextureFileName);

                        if (i < _materials.Count - 1)
                            file.Write(",");
                    }
                }
                file.Close();
            }
        }

        private string WriteHeader()
        {
            string ret = "xof 0303txt 0032\n" +
                "template Vector {\n" +
                " <3d82ab5e-62da-11cf-ab39-0020af71e433>\n" +
                " FLOAT x;\n" +
                " FLOAT y;\n" +
                " FLOAT z;\n" +
                "}\n" +
                "\n" +
                "template MeshFace {\n" +
                " <3d82ab5f-62da-11cf-ab39-0020af71e433>\n" +
                " DWORD nFaceVertexIndices;\n" +
                " array DWORD faceVertexIndices[nFaceVertexIndices];\n" +
                "}\n" +
                "\n" +
                "template Mesh {\n" +
                " <3d82ab44-62da-11cf-ab39-0020af71e433>\n" +
                " DWORD nVertices;\n" +
                " array Vector vertices[nVertices];\n" +
                " DWORD nFaces;\n" +
                " array MeshFace faces[nFaces];\n" +
                " [...]\n" +
                "}\n" +
                "\n" +
                "template MeshNormals {\n" +
                " <f6f23f43-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nNormals;\n" +
                " array Vector normals[nNormals];\n" +
                " DWORD nFaceNormals;\n" +
                " array MeshFace faceNormals[nFaceNormals];\n" +
                "}\n" +
                "\n" +
                "template Coords2d {\n" +
                " <f6f23f44-7686-11cf-8f52-0040333594a3>\n" +
                " FLOAT u;\n" +
                " FLOAT v;\n" +
                "}\n" +
                "\n" +
                "template MeshTextureCoords {\n" +
                " <f6f23f40-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nTextureCoords;\n" +
                " array Coords2d textureCoords[nTextureCoords];\n" +
                "}\n" +
                "\n" +
                "template Material {\n" +
                " <3d82ab4d-62da-11cf-ab39-0020af71e433>\n" +
                " ColorRGBA faceColor;\n" +
                " FLOAT power;\n" +
                " ColorRGB specularColor;\n" +
                " ColorRGB emissiveColor;\n" +
                " [...]\n" +
                "}\n" +
                "\n" +
                "template MeshMaterialList {\n" +
                " <f6f23f42-7686-11cf-8f52-0040333594a3>\n" +
                " DWORD nMaterials;\n" +
                " DWORD nFaceIndexes;\n" +
                " array DWORD faceIndexes[nFaceIndexes];\n" +
                " [Material <3d82ab4d-62da-11cf-ab39-0020af71e433>]\n" +
                "}\n" +
                "\n" +
                "template TextureFilename {\n" +
                " <a42790e1-7810-11cf-8f52-0040333594a3>\n" +
                " STRING filename;\n" +
                "}\n";
            return ret;
        }

        /// <summary>
        /// Method writes the provided <see cref="Model"/> to a directX file (.x) 
        /// </summary>
        /// <param name="model">The <see cref="Model"/> to be processed.</param>
        /// <param name="number">The <see cref="Model"/>'s index in the <see cref="_modelsList"/></param>
        /// <remarks>The name of the file is constructed using the material of the model , and the model's index in the 
        /// <see cref="_modelsList"/></remarks>
        private void WriteModelFile(Model model, int number)
        {
            int vertexIndex = model.model1.VertexIndex;
            int vertexOffset = model.model1.VertexOffset;
            int vertexLength = model.model1.VertexLenght;

            int indexIndex = model.model1.IndiceIndex;
            int indexOffset = model.model1.IndiceOffset;
            int indexLength = model.model1.IndiceLenght;

            string filename = Helper.SpellcheckDirectory(ModelOutputDir) + _materials[model.Material].MaterialName + number + ".x";

            using (StreamWriter file = new StreamWriter(filename))
            {
                file.Write(_header);
                file.Write("Mesh {\n");
                file.Write(" " + vertexLength + ";\n");

                // Vertices
                for (int j = vertexOffset; j < vertexOffset + vertexLength; j++)
                {
                    if (j < vertexOffset + vertexLength - 1)
                    {
                        file.Write(" " + _verticeLists[vertexIndex].Vertices[j].Position[0] + ";");
                        file.Write(_verticeLists[vertexIndex].Vertices[j].Position[1] + ";");
                        file.Write(_verticeLists[vertexIndex].Vertices[j].Position[2] + ";,\n");
                    }
                    else
                    {
                        file.Write(" " + _verticeLists[vertexIndex].Vertices[j].Position[0] + ";");
                        file.Write(_verticeLists[vertexIndex].Vertices[j].Position[1] + ";");
                        file.Write(_verticeLists[vertexIndex].Vertices[j].Position[2] + ";;\n\n");
                    }
                }

                // Indices
                file.Write(" " + (indexLength / 3) + ";\n");
                for (int j = indexOffset; j < indexOffset + indexLength; j += 3)
                {
                    if (j < indexOffset + indexLength - 3)
                    {
                        file.Write(" 3;" + (_indexesLists[indexIndex].Indexes[j] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 1] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 2] - vertexOffset) + ";,\n");
                    }
                    else
                    {
                        file.Write(" 3;" + (_indexesLists[indexIndex].Indexes[j] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 1] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 2] - vertexOffset) + ";;\n\n");
                    }
                }

                // Normals
                file.Write(" MeshNormals {\n  " + vertexLength + ";\n");
                for (int j = vertexOffset; j < vertexOffset + vertexLength; j++)
                {

                    if (_verticeLists[vertexIndex].Vertices[j] is VerticeWithUVNormals)
                    {
                        VerticeWithUVNormals vert = _verticeLists[vertexIndex].Vertices[j] as VerticeWithUVNormals;
                        if (vert != null)
                        {
                            if (j < vertexOffset + vertexLength - 1)
                            {
                                file.Write("  " + vert.Normals[0] + ";");
                                file.Write(vert.Normals[1] + ";");
                                file.Write(vert.Normals[2] + ";,\n");
                            }
                            else
                            {
                                file.Write("  " + vert.Normals[0] + ";");
                                file.Write(vert.Normals[1] + ";");
                                file.Write(vert.Normals[2] + ";;\n\n");
                            }
                        }
                    }
                }

                // Normals indices
                file.Write("  " + (indexLength / 3) + ";\n");
                for (int j = indexOffset; j < indexOffset + indexLength; j += 3)
                {
                    if (j < indexOffset + indexLength - 3)
                    {
                        file.Write("  3;" + (_indexesLists[indexIndex].Indexes[j] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 1] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 2] - vertexOffset) + ";,\n");
                    }
                    else
                    {
                        file.Write("  3;" + (_indexesLists[indexIndex].Indexes[j] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 1] - vertexOffset) + ",");
                        file.Write((_indexesLists[indexIndex].Indexes[j + 2] - vertexOffset) + ";;\n }\n\n");
                    }
                }

                //texture coodrinates
                file.Write(" MeshTextureCoords {\n  " + vertexLength + ";\n");
                for (int j = vertexOffset; j < vertexOffset + vertexLength; j++)
                {
                    if (_verticeLists[vertexIndex].Vertices[j] is VerticeWithUVNormals)
                    {
                        VerticeWithUVNormals vert = _verticeLists[vertexIndex].Vertices[j] as VerticeWithUVNormals;
                        if (vert != null)
                        {
                            if (j < vertexOffset + vertexLength - 1)
                            {
                                file.Write("  " + vert.UV[0] + ";");
                                file.Write(vert.UV[1] + ";,\n");
                            }
                            else
                            {
                                file.Write("  " + vert.UV[0] + ";");
                                file.Write(vert.UV[1] + ";;\n }\n\n");
                            }
                        }
                    }
                }

                //material list
                file.Write(" MeshMaterialList {\n  1;\n  1;\n  0;\n\n");
                //material
                file.Write("  Material {\n");
                file.Write("   1.000000;1.000000;1.000000;1.000000;;\n");
                file.Write("   3.200000;\n");
                file.Write("   0.000000;0.000000;0.000000;;\n");
                file.Write("   0.000000;0.000000;0.000000;;\n\n");
                //texutre
                file.Write("   TextureFilename {\n");
                file.Write("    \"texture\\\\" + _materials[model.Material].TextureFileName + "\";\n   }\n  }\n }\n}");
            }
        }

        private void CreateAndExecuteAssetViewerBatch()
        {
            string scriptPath = Path.Combine(Environment.CurrentDirectory, "ConvertToObj.bat");
            CreateScript(scriptPath);

            Console.WriteLine("Conversion script generation complete.");

            // Execute script
            if (!Directory.Exists(_objOutputDir))
            {
                Directory.CreateDirectory(_objOutputDir);
            }

            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();

            startInfo.FileName = @"C:\Windows\System32\cmd.exe";
            startInfo.Arguments = string.Format("/C \"{0}\"", scriptPath); 

            process.StartInfo = startInfo;
            process.Start();
        }

        /// <summary>
        /// Method ensures that all files and folders used in the process exist, also , if some of the parameters are missing 
        /// it resets them to default values.
        /// </summary>
        /// <returns>true if the all the conditions for running are satisfied, otherwise false.</returns>
        private bool ValidateParameters()
        {
            if (string.IsNullOrEmpty(InputFileName))
            {
                Console.WriteLine("Path to .nvr file not specified. Please retry with specifying the path.");
                ModelOutputDir = Environment.CurrentDirectory;
            }


            if (string.IsNullOrEmpty(ModelOutputDir))
            {
                Console.WriteLine("Model output directory was not provided. Outputing in the current directory.");
                ModelOutputDir = Environment.CurrentDirectory;
            }

            if (!Directory.Exists(ModelOutputDir))
            {
                Directory.CreateDirectory(ModelOutputDir);
            }

            ModelOutputDir = Helper.SpellcheckDirectory(ModelOutputDir);

            if (!File.Exists(AssetViewerPath))
            {
                Console.WriteLine("No valid path to the Asset Viewer Command line tool. (" + AssetViewerPath + ")");
                Console.WriteLine("Switching to the embedded asset viewer.(version 3.0)");
                return UseEmbeddedAssetViewer();
            }

            return true;
        }

        /// <summary>
        /// Method Checks for avialable embedded Asset viewer auto-detecting the environment architecure
        /// on which this app is executed (x86, x64 ...).  
        /// </summary>
        /// <returns>true if embedded Asset Viewer can be found, false otherwise</returns>
        /// <remarks>This method is only called if no AssetViewerPath is provided with parameters.
        /// Returning false by this method exits the application.</remarks>
        private bool UseEmbeddedAssetViewer()
        {
            AssetViewerPath = Helper.GetEmbeddedAssetViewerPath();
            Console.WriteLine("Asset viewer found at : " + AssetViewerPath);
            return (!string.IsNullOrEmpty(AssetViewerPath));
        }

        
        /// <summary>
        /// Method creates the script for converting the DirectX file (.x) to .obj
        /// </summary>
        /// <param name="scriptPath">the path of the script.</param>
        private void CreateScript(string scriptPath)
        {
            // Generate script 
            Console.WriteLine("Generating conversion script : " + scriptPath);
            Console.WriteLine("Start copying to  " + _objOutputDir);
            Console.Write("Working:");

            using (StreamWriter file = new StreamWriter(scriptPath))
            {
                file.WriteLine("@ECHO OFF");
                file.WriteLine("@ECHO Conversion script running now.\n");
                file.WriteLine("FOR /R \"" + ModelOutputDir + "\" %%G IN (*.x) DO (");
                file.WriteLine("\"" + AssetViewerPath + "\"" + " export %%G \"" + _objOutputDir + "%%~nG.obj\" -fobj");
                file.WriteLine(")");

                file.WriteLine("\n@ECHO All DirectX files(.x) converted to Obj");
                file.WriteLine("set /p id=\"Press enter to close. %=%");
            }
        }
    }
}
