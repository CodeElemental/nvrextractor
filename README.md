# Nvr Extractor

This is a small Windows console application that extracts the models from the .nvr file and converts them to .obj using third party open source app [Assimp Viewer](http://assimp.sourceforge.net/main_viewer.html).